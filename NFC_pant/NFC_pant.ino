/*
 * Pin layout should be as follows:
 * Signal     Pin              Pin
 *            Arduino Uno      MFRC522 board
 * ------------------------------------------
 * Reset      9                RST
 * SPI SDA    10              SDA
 * SPI MOSI   11               MOSI
 * SPI MISO   12               MISO
 * SPI SCK    13               SCK
 */

#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>

#include <SPI.h>
#include <MFRC522.h>

#define SOUND 6
#define SS_PIN 10
#define RST_PIN 9
MFRC522 mfrc522(SS_PIN, RST_PIN);	// Create MFRC522 instance.
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); // Addr, En, Rw, Rs, d4, d5, d6, d7, backlighpin, polarity

String prevUID = "";
String currentMessage = "";

float total = 0;
bool print = false;
bool first = true;

void setup() {
	Serial.begin(9600);	// Initialize serial communications with the PC
	Serial.flush();
    SPI.begin();			// Init SPI bus
	mfrc522.PCD_Init();	// Init MFRC522 card
	lcd.begin(16,2);
	lcd.backlight();
	Serial.println("Ready to scan...\n");
}

void loop() {
        // Look for new cards<

	if(print) {
		lcd.clear();
		lcd.print(currentMessage);
		if(total != 0) {
			lcd.setCursor(0,1);
			lcd.print("Total: " + String(total));
		}
		print = false;
	}
	else if (first) {
		lcd.clear();
		lcd.print("Scan something");
		lcd.setCursor(0,1);
		lcd.print("to begin...");
		first = false;
	}
	

	if ( ! mfrc522.PICC_IsNewCardPresent()) {
		return;
	}
	// Select one of the cards
	if ( ! mfrc522.PICC_ReadCardSerial()) {
		return;
	}
	String UID = getUID();
	
	if(UID != prevUID) {
	 	//logCardData();
	 	//authenticateMifare();
		readAndDo();
		//writePant(200,0,2);
		//cleanUltralightChip();
		prevUID = UID;
	}
}

void readAndDo() {
	byte type[4];
	byte price[4];
	byte verify[4];
	bool pant = false;

	readUltralightBlock(4,&type[0]);
	readUltralightBlock(5,&price[0]);
	readUltralightBlock(6,&verify[0]);
	
	String valueString = "";
	Serial.println();

	if(verify[0] != 1 || verify[1] != 1 || verify[2] != 1 || verify[3] != 1) {
		Serial.println("This tag has already been scanned,");
		currentMessage = "Not valid...";
		print = true;
		return;
	} else {
		if(type[3] == 1) {
			Serial.println("It's a bottle.");
			currentMessage = "Bottle ";
			pant = true;
		}
		else if(type[3] == 2) {
			Serial.println("It's a can.");
			currentMessage = "Can ";
			pant = true;
		} else {
			Serial.println("WTF?");
			currentMessage = "ERROR ASSHOLE!";
			tone(SOUND,4000,1000);
			print = true;
		}
	}

	if(pant) {
			tone(SOUND,1900,200);
			valueString += price[2];
			valueString += ".";
			valueString += price[3];
			Serial.println("Value is " + valueString + " kr");
			currentMessage = currentMessage + valueString + " kr";

			double kr = price[2];
			double ore = price[3]/10.0;
			total = total + kr + ore;

			Serial.println("The pant has now been used.");
			print = true;
			//emptyVerify();
	}

}

	// Block 4 is type
	// 00 00 00 00 = Empty
	// 00 00 00 01 = Bottle
	// 00 00 00 02 = Can

	// Block 5 is price
	// 00 00 00 00 = 0 kr
	// 00 00 10 00 = 16 kr
	// 00 00 02 50 = 2.50 kr

	// Block 6 is to verify
	// 00 00 00 00 = Can be scanned
	// 01 01 01 01 = Is scanned

void writePant(int kr, int ore, byte type) {
	cleanUltralightChip();

	// Write type
	byte typeBlock[] = {0,0,0,type};
	writeUltralightBlock(4, typeBlock);

	// Write price
	byte price[] = {0,0,kr,ore};
	writeUltralightBlock(5, price);

	// Verify
	byte verify[] = {1,1,1,1};
	writeUltralightBlock(6, verify);

	if(type == 1) {
		Serial.println("Tag is now a bottle");
	}
	else if(type == 2) {
		Serial.println("Tag is now a can");
	}
	
}

void fillVerify() {
	byte verify[] = {1,1,1,1};
	writeUltralightBlock(6, verify);
}

void emptyVerify() {
	byte verify[] = {0,0,0,0};
	writeUltralightBlock(6, verify);
}