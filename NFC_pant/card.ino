bool auth = false;


void logCardData() {
// Logs a card if selected
	mfrc522.PICC_DumpToSerial(&(mfrc522.uid));
}

String getUID()
{
// Dump UID
	String UID = "";
	for (byte i = 0; i < mfrc522.uid.size; i++) {
		UID += mfrc522.uid.uidByte[i];
	}
	return UID;
}

void dumpType()
{
// Dump PICC type
	byte piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
	Serial.print("PICC type: ");
	Serial.println(mfrc522.PICC_GetTypeName(piccType));
	if (        piccType != MFRC522::PICC_TYPE_MIFARE_MINI 
		&&        piccType != MFRC522::PICC_TYPE_MIFARE_1K
		&&        piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
		return;
} 
}

byte getPicc() {
	byte piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
	return piccType;
}

// Ultralight functions

int writeUltralightBlock(byte block, byte values[]) {
	byte status = mfrc522.MIFARE_Write(block, values, 16);
	if (status != MFRC522::STATUS_OK) {
		Serial.print("MIFARE_Write() failed: ");
		Serial.println(mfrc522.GetStatusCodeName(status));
                return 0;
	} else 
	{
		String message = "Block ";
		message += block;
		message += " has been written";
		Serial.println(message);
                return 1;
	}
}

void readUltralightBlock(byte block, byte *data) {
	byte buffer[18];
    byte size = sizeof(buffer);

    byte status = mfrc522.MIFARE_Read(block, buffer, &size);
    byte piccType = getPicc();

    if(piccType == MFRC522::PICC_TYPE_MIFARE_UL) {
    	for(int i = 0; i < 4; i++) {
    		data[i] = buffer[i];
    	}
    }
}

int writeMifareBlock(byte block, byte values[]) {
	byte buffer[18];
	byte size = sizeof(buffer);
	byte status;

	
}

void readMifareBlock(byte block, byte *data) {
	byte buffer[18];
    byte size = sizeof(buffer);

    if(!auth) {
		authenticateMifare();
	}

	byte status = mfrc522.MIFARE_Read(block, buffer, &size);

	byte piccType = getPicc();

    if(piccType == MFRC522::PICC_TYPE_MIFARE_1K) {
    	Serial.println("Reading...");
    	for(int i = 0; i < 16; i++) {
    		data[i] = buffer[i];
    	}
    }

}

void authenticateMifare() {
	MFRC522::MIFARE_Key key;
	for (byte i = 0; i < 6; i++) {
	        key.keyByte[i] = 0xFF;
	}

	byte status;
	byte trailerBlock   = 7;


	// Authenticate using key A.
        Serial.println("Authenticating using key A...");
        status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
        if (status != MFRC522::STATUS_OK) {
                Serial.print("PCD_Authenticate() failed: ");
                Serial.println(mfrc522.GetStatusCodeName(status));
                return;
        } else {
        	Serial.println("Auth A success");
        }
        // Authenticate using key B.
        Serial.println("Authenticating again using key B...");
        status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, trailerBlock, &key, &(mfrc522.uid));
        if (status != MFRC522::STATUS_OK) {
                Serial.print("PCD_Authenticate() failed: ");
                Serial.println(mfrc522.GetStatusCodeName(status));
                return;
        } else {
        	Serial.println("Auth B success");
        }

        auth = true;
}

void cleanUltralightChip() {
	byte block = 0;
	byte values[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	for(int i = 4; i < 16; i++) {
		block = i;
		writeUltralightBlock(block, values);
	}
	Serial.println("Cleaned chip");
	//logCardData();
}

